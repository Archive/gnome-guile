(use-modules (gnome gnome)
	     (gtk gtk)
	     (gtk libglade))

(define (test)
  (gnome-init-hack "test" #f '())
  (glade-gnome-init)
  
  (let* ((xml (glade-xml-new "project2.glade"))
	 (window (glade-xml-get-widget xml "app1"))
	 (text (glade-xml-get-widget xml "text1"))
	 (entry (glade-xml-get-widget xml "entry1"))
	 (button1 (glade-xml-get-widget xml "button6")))

    (glade-xml-signal-autoconnect xml)

    (gtk-signal-connect button1 "clicked"
			(lambda()
			  (gtk-text-insert text #f "black" #f (gtk-entry-get-text entry) -1)))

    (gtk-widget-show-all window)
    (gtk-main)))

(test)
    



















