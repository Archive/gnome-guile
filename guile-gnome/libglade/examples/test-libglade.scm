(use-modules (gnome gnome)
	     (gtk gtk)
             (gtk libglade))

(define (test file type)
  (if type
      (begin 
	(gnome-init-hack "test" #f '())
	(glade-gnome-init))
      (glade-init))

  (let* ((xml (glade-xml-new file))
	 (window (glade-xml-get-widget xml "window1")))

    (glade-xml-signal-autoconnect xml)
    (gtk-widget-show-all window)

    (gtk-main)))
   
;(test "button.glade" #f)
(test "example.glade" #t)
;(test "test.glade" #f)



