;;Originally coded by B.Knotwell and adapted for galway by me =)
;;Somehow I need to rewritte this header... =)
;;At least it is know working... And working correctly and fast
;;Gone are those 15 seconds...


;;; general functions that should (in some way) be a part of guile
(define (err-and-exit data) (begin (error data "") (exit)))

(define (neq? x . y) (not (apply eq? x y)))

(define (private-readable mode what)
  (cond ((eq? what 'user) (> (logand 00400 mode) 0))
	((eq? what 'group) (> (logand 00040 mode) 0))
	(else (> (logand 00004 mode) 0))))

(define (private-writable mode what)
  (cond ((eq? what 'user) (> (logand 00200 mode) 0))
	((eq? what 'group) (> (logand 00020 mode) 0))
	(else (> (logand 00002 mode) 0))))

(define (private-executable mode what)
  (cond ((eq? what 'user) (> (logand 00100 mode) 0))
	((eq? what 'group) (> (logand 00010 mode) 0))
	(else (> (logand 00001 mode) 0))))

(define (file-perms name)
  (define mystat 
    (if (file-exists? name) (stat name) #f))
  (let 	((mode (stat:mode mystat)) (gid (stat:gid mystat))
	(uid (stat:uid mystat))    (myuid (getuid))        (mygid (getgid)))
    (cond 
     ((boolean? mystat) (error "file does not exist" ""))
     ((eq? myuid uid) (map (lambda (x) (x mode 'user)) (list private-readable private-writable private-executable)))
     ((eq? mygid gid) (map (lambda (x) (x mode 'group)) (list private-readable private-writable private-executable)))
     (else map (lambda (x) (x mode 'default)) (list private-readable private-writable private-executable)))))

(define (isreadable? filename)                  ;;I really love this permissions parts
  (car (file-perms filename)))                  ;;IMHO it boosts galway...
                                                ;;maybe I should extend it so the user can see
(define (iswritable? filename)                  ;; a pop up dialog...
  (cadr (file-perms filename)))

(define (isexecutable? filename)
  (caddr (file-perms filename)))

(define (readlines filename)
  (define (readlines-iter myfile result)
    (let ((myread (read-line myfile)))
      (if (eof-object? myread)
           (list result myfile)
          (readlines-iter myfile (append result (list myread)) ))))
  (if (not (and (file-exists? filename) (isreadable? filename)))
      #f
      (let ((a (open-input-file filename)))
             (readlines-iter a '() ))))

(define (insert-quote data)
  (list->string (map (lambda (x) (if (eq? x #\") (string-append "\\" "\"") x)) (string->list data))))

(define (write-to-outfile outfile data)
  (map (lambda (x) (write-line x outfile)) data))

(define (txt->string-file inputfile) ;;I think this name is better...
 (let* ((data-l (readlines inputfile))
        (data (car data-l)) (port (cadr data-l))
        (name (string-append inputfile ".tmp"))
        (myoutfile (open-output-file name)))
	  (cond ((boolean? data) (err-and-exit "problem opening input file"))
		((boolean? myoutfile) (err-and-exit "problem opening output file"))
		(else (write-line "\"" myoutfile)
		      (write-to-outfile myoutfile (map insert-quote data))
		      (write-line "\"" myoutfile)))
       (close-input-port port)          ;;This lines were added because I
       (close-output-port myoutfile)    ;;want to follow the hack guide and to avoid problem
       (let* ((a (open-input-file name));;with this one =)
             (b (read a)))            
             (close-input-port a) 
             (delete-file name) b)))   ;;the temp file is deleted... avoiding a security hazard





