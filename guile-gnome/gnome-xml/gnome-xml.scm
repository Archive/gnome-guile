(define-module (gnome gnome-xml)
  :use-module (gnome gnome)
  :use-module (gtk gtk)
  :use-module (gtk dynlink))

(merge-compiled-code "libgnomexml_init_glue" "libguilegnomexml")

