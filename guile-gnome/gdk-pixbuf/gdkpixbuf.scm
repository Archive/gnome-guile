(define-module (gtk gdkpixbuf)
  :use-module (gtk gtk)
  :use-module (gtk gdk)
  :use-module (gnome gnome)
  :use-module (gtk dynlink))

(merge-compiled-code "libguilegdkpixbuf_init_glue" "libguilegdkpixbuf")

