/* -*- mode: c; c-basic-offset: 8 -*- */

#include <libguile.h>
#include <guile-gtk.h>
#include <gnome.h>
#include <liboaf/liboaf.h>
#include <orb/orbit.h>

void *
oaf_corba_orb_copy (void *orb)
{
    ORBIT_ROOT_OBJECT_REF (orb);
    return orb;
}

void  
oaf_corba_orb_free (void *orb)
{
    ORBIT_ROOT_OBJECT_UNREF (orb);
}

void *
oaf_corba_env_copy (void *env)
{
	return env;
}

void
oaf_corba_env_free (void *env)
{
}
