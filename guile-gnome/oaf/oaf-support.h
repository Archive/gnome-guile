/* -*- mode: c; c-basic-offset: 8 -*- */

#ifndef OAF_SUPPORT_H
#define OAF_SUPPORT_H

void *oaf_corba_orb_copy (void *orb);
void oaf_corba_orb_free (void *orb);

void * oaf_corba_env_copy (void *env);
void oaf_corba_env_free (void *env);

#endif
