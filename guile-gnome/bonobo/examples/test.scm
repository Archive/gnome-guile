;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ariel Rios                        ;;
;; ariel@linuxppc.org                ;;
;; http://linux.cem.itesm.mx/~ariel/ ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Licensed under the GPL
;; Copyright Ariel Rios 2001

;; Shamelessly translation of
;; sample-control-container into Scheme
;; minus the horrible Michael C coding style ;)

(use-modules (gtk gtk)
	     (gnome gnome)
	     (gnome oaf)
	     (gnome bonobo))

;; Some standard components

(define nautilus-shell "OAFIID:nautilus_shell:cd5183b2-3913-4b74-9b8e-10528b0de08d")
(define gtkhtml-component "OAFIID:GNOME_GtkHTML_Editor")
(define gtkhtml-ebrowser "OAFIID:GNOME_GtkHTML_EBrowser")
(define ggv "OAFIID:bonobo_application-ps:GGV20001224")
(define eog "OAFIID:GNOME_EOG_Control")
(define evo-abook "OAFIID:GNOME_Evolution_Addressbook_Control")

;; Change the value here to play with the differente components

(define kstr gtkhtml-component)

;; The real thingie

(define (container-create)
  (gtk-idle-remove idle-flag)
  (let* ((app (bonobo-window-new "sample-control-container" "Sample Scheme Bonobo Control Container"))
	 (uic (bonobo-ui-container-new))
	 (box (gtk-vbox-new #f 0))
	 (control (bonobo-widget-new-control kstr (guile-bonobo-obj-ref uic)))) ;; equiv to doing BONOBO_OBJREF (uic)

    (bonobo-ui-container-set-win uic app)
    (bonobo-window-set-contents app box)
    
    (gtk-box-pack-start box control #t #t 0)

    (gtk-signal-connect app "destroy" (lambda () (gtk-main-quit)))

    (gtk-window-set-default-size app 500 440)
    (gtk-window-set-policy app #t #t #f)

    (gtk-widget-show-all app)))
	
(gnome-init-hack "baboon" #f '())


(define orb (oaf-init '()))

(define idle-flag #f)

(if (bonobo-init orb)
    (begin 
      (set! idle-flag (gtk-idle-add-full 0 container-create))
      (bonobo-main))
    (display "oops! Error starting bonobo-init"))
     
(gtk-main)

