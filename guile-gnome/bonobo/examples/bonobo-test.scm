(use-modules (gtk gtk)
	     (gnome gnome)
	     (gnome gnorba)	    
	     (gnome bonobo))

(debug-enable 'debug 'backtrace)

(define (container-create)
  (let* ((app (gnome-app-new "guile-html-control" "HTML test"))
	 (uih (bonobo-ui-handler-new))
	 (control (bonobo-widget-new-control "control:html-editor" (bonobo-object-corba-objref uih))))
    (gnome-app-set-contents app control)
    (gtk-widget-show-all app)))
  
(define (init-corba)
;  (corba-exception-init)
  (gnome-corba-init "guile-html-control" "1.0" '() 'init-server-func))

(define (init-bonobo)
  (init-corba)
  #t)
;;  (equal? (bonobo-init 'object-nil  'object-nil 'object-nil))) 

(define (main)
  (call-with-current-continuation
   (lambda (break)
     (if (not (init-bonobo))
	 (break #f))
     (container-create)
     (bonobo-main))))

(main)