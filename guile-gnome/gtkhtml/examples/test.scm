#! /bin/sh
exec guile-gtk -s $0 $*
!#

;;;;;;;;;;;;;;;;;;;;;;;;
;;Ariel Rios          ;;
;;ariel@arcavia.com   ;;
;;gtkhtml example     ;;
;;Febrero 5, 2001     ;;
;;;;;;;;;;;;;;;;;;;;;;;;

;;Released under GPL;;

(use-modules (gtk gtk)
	     (gtk gdk)
             (gtk gtkhtml))


(gdk-rgb-init)

(gtk-widget-set-default-colormap (gdk-rgb-get-cmap))
(gtk-widget-set-default-visual (gdk-rgb-get-visual))

(define (test)
  
  (let* ((window (gtk-window-new 'toplevel))
	 (box (gtk-vbox-new #f 0))
	 (swin (gtk-scrolled-window-new #f #f))
	 ;;(handle (gtk-html-begin html))
         (buffer "<HTML><HEAD><TITLE>hello world</TITLE></HEAD><BODY>Hello World</BODY></HTML>")
	 (n (string-length buffer))
	 (html (gtk-html-new-from-string buffer n)))

    (gtk-scrolled-window-set-policy swin 'automatic 'automatic)
    (gtk-container-add swin html)
    (gtk-container-add window swin)
    (gtk-widget-realize html)
    (gtk-widget-set-usize window 500 400)
    (gtk-widget-show-all window)
    
    (gtk-standalone-main window)))

(test)

