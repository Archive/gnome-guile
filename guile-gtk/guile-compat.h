#ifndef GUILE_COMPAT_H
#define GUILE_COMPAT_H

#include <libguile.h>

void scm_done_malloc (long size);
SCM scm_internal_cwdr (scm_catch_body_t body,
		       void *body_data,
		       scm_catch_handler_t handler,
		       void *handler_data,
		       SCM_STACKITEM *stack_start);
void scm_puts (char *str, SCM port);

#ifndef SCM_LIST1
#define SCM_LIST1(e0) scm_cons ((e0), SCM_EOL)
#endif

#ifndef SCM_LIST2
#define SCM_LIST2(e0, e1) scm_cons2 ((e0), (e1), SCM_EOL)
#endif

#ifndef HAVE_SCM_REVERSE_X
#define scm_reverse_x scm_list_reverse_x
#endif

#endif
