#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="Gnome Guile Libraries"

(test -f $srcdir/configure.in \
  && test -d $srcdir/guile-gtk \
  && test -d $srcdir/guile-gnome) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level gnome-guile directory"
    exit 1
}

USE_GNOME2_MACROS=1 . gnome-autogen.sh

